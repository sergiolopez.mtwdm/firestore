package com.example.firebaseintro

import com.google.firebase.firestore.QuerySnapshot

fun QuerySnapshot.toListOfBooks():List<MainActivity.Book>{
    var lst = mutableListOf<MainActivity.Book>()

    for(document in this){
        lst.add(document.toObject(MainActivity.Book::class.java))
    }

    return lst
}