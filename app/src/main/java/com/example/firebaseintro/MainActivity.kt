package com.example.firebaseintro

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.FirebaseFirestore

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val TAG = "FirebaseDebug"

        //populateBooks()


        booksRef().get().addOnSuccessListener { result ->
            var books = result.toListOfBooks()
            Log.d(TAG, books.toString())

            /*for (document in result) {
                var book = document.toObject(Book::class.java)
                //book.id = document.id
                Log.d(TAG, " ID ${book.id}")
                Log.d(TAG, " Name ${book.name}")
                Log.d(TAG, " Description ${book.description}")
            }*/
        }


/*

        val db = FirebaseFirestore.getInstance()
        Log.d(TAG, "Get all documents")
        db.collection("books")
            .get()
            .addOnSuccessListener { result ->
                for(document in result){
                    Log.d(TAG, " Name ${document.getString("name")}")
                    Log.d(TAG, " Description ${document.getString("description")}")
                }
            }
            .addOnFailureListener {

            }

 */
        /*
        val db = FirebaseFirestore.getInstance()
        db.collection("books")
            .addSnapshotListener { value, error ->
                value.let {
                    for (document in value?.documents!!)
                    {
                        Log.d(TAG, " Name ${document?.getString("name")}")
                        Log.d(TAG, " Description ${document?.getString("description")}")
                    }
                }

            }

         */

        /*
        db.collection("books")
            .document("001")
            .addSnapshotListener { value, error ->
                value.let{ document ->
                    document.let{
                        Log.d(TAG, " Name ${document?.getString("name")}")
                        Log.d(TAG, " Description ${document?.getString("description")}")
                    }
                }
            }

         */

        /*
        db.collection("books")
            .document()
            .set(Book(name = "Programing in Go", description = "Learn Go"))
            .addOnSuccessListener {
                Log.d(TAG, "Book Saved")
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "An error ocurred")
            }

         */

        /*
        db.collection("books")
            .whereEqualTo("name", "Programming in C++")
            .get()
            .addOnSuccessListener { result ->
                for(document in result){
                    Log.d(TAG, " Name ${document.getString("name")}")
                    Log.d(TAG, " Description ${document.getString("description")}")
                }
            }
         */

        /*
        db.collection("books")
            .document("001")
            .get()
            .addOnSuccessListener { document ->
                document.let{
                    Log.d(TAG, "Name ${document.getString("name")}")
                    Log.d(TAG, "Description ${document.getString("description")}")
                }
            }

         */


    }

    fun populateBooks() {
        bookRef().set(Book(name = "Ejemplo1", description = "descripcion"))
        bookRef().set(Book(name = "Ejemplo2", description = "descripcion"))
        bookRef().set(Book(name = "Ejemplo3", description = "descripcion"))
        bookRef().set(Book(name = "Ejemplo4", description = "descripcion"))
    }

    fun collectionRef(collection: String) = FirebaseFirestore.getInstance().collection(collection)
    fun booksRef() = collectionRef("books")
    fun bookRef() = booksRef().document()

    data class Book(
        @DocumentId
        var id: String = "",
        var name: String = "",
        var description: String = ""
    )
}